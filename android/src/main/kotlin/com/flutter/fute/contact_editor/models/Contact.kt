package com.flutter.fute.contact_editor.models

import android.graphics.Bitmap
import android.net.Uri
import com.flutter.fute.contact_editor.androidcontacts.entity.*
import java.util.*

data class Contact(
        val contactId: Int = 0 ,
        val lookupKey: String = "" ,
        //val emailList: List<Email?>? = ArrayList() ,
        //val phoneList: List<PhoneNumber?>? = ArrayList() ,
        //val addressesList: List<Address?>? = ArrayList() ,
        //val websitesList: List<String?>? = ArrayList() ,
        //val imAddressesList: List<IMAddress?>? = ArrayList() ,
        //val relationsList: List<Relation?>? = ArrayList() ,
        //val specialDatesList: List<SpecialDate?>? = ArrayList() ,
        //val groupList: List<Group?>? = ArrayList() ,
        val note: String = "" ,
        val nickName: String = "" ,
        val sipAddress: String = "" ,
        //val photoUri: Uri? = Uri.EMPTY ,
        //val organization: Organization? = Organization() ,
        //val nameData: NameData? = NameData() ,
        val compositeName: String = "" ,
        val accountName: String = "" ,
        val accountType: String = "" ,
        //val lastModificationDate: Long = 0 ,
        //val updatedPhotoUri: Uri? = null ,
        //val updatedBitmap: Bitmap? = null ,
        val isFavorite: Boolean = false
)
