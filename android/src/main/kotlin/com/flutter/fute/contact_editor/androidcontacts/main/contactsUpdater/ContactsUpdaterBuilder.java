package com.flutter.fute.contact_editor.androidcontacts.main.contactsUpdater;

import android.content.Context;
import com.flutter.fute.contact_editor.androidcontacts.entity.ContactData;

import java.util.Collections;
import java.util.List;

public class ContactsUpdaterBuilder {
    private Context mCtx;

    public ContactsUpdaterBuilder(Context mCtx) {
        this.mCtx = mCtx;
    }

    public int[] updateContactsList(List<ContactData> contactDataList) {
        return new ContactsUpdater(mCtx.getContentResolver())
                .updateContacts(contactDataList);
    }

    public int updateContact(ContactData contactData) {
        List<ContactData> contactDatas = Collections.singletonList(contactData);
        int[] ids = new ContactsUpdater(mCtx.getContentResolver())
                .updateContacts(contactDatas);
        return ids[0];
    }
}
