package com.flutter.fute.contact_editor

import android.app.Activity
import android.content.Context
import android.content.ContentResolver
import android.content.ContentValues
import android.provider.ContactsContract
import android.util.Log
import androidx.annotation.NonNull
import com.flutter.fute.contact_editor.androidcontacts.entity.ContactData
import com.flutter.fute.contact_editor.androidcontacts.entity.PhoneNumber
import com.flutter.fute.contact_editor.androidcontacts.main.contactsGetter.ContactsGetterBuilder
import com.flutter.fute.contact_editor.androidcontacts.main.contactsUpdater.ContactsUpdaterBuilder
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar
import io.reactivex.Observable
import io.reactivex.disposables.Disposables
import java.security.AccessController
import java.util.*
import java.util.stream.Collectors
import java.util.HashMap
import kotlin.collections.ArrayList

import com.flutter.fute.contact_editor.models.Contact

import java.util.Collections

import java.util.Comparator

import android.os.Build

import android.annotation.TargetApi

import android.os.AsyncTask
import java.util.concurrent.ArrayBlockingQueue

import java.util.concurrent.ThreadPoolExecutor

import java.util.concurrent.ExecutorService
import java.util.concurrent.TimeUnit


/** ContactEditorPlugin */
class ContactEditorPlugin : FlutterPlugin, MethodCallHandler, ActivityAware {

    private val executor: ExecutorService = ThreadPoolExecutor(0, 10, 60, TimeUnit.SECONDS, ArrayBlockingQueue(1000))

    private lateinit var context: Context
    private lateinit var activity: Activity

    /// The MethodChannel that will the communication between Flutter and native Android
    ///
    /// This local reference serves to register the plugin with the Flutter Engine and unregister it
    /// when the Flutter Engine is detached from the Activity
    private lateinit var channel: MethodChannel

    //private lateinit var contactsHelper: ContactsHelper;
    private var disposable = Disposables.empty();

    override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        channel = MethodChannel(flutterPluginBinding.binaryMessenger, "contact_editor")
        channel.setMethodCallHandler(this)
        context = flutterPluginBinding.applicationContext
    }

    @TargetApi(Build.VERSION_CODES.ECLAIR)
    private fun getContacts(result: Result) {
        GetContactsTask(result).executeOnExecutor(executor, context);
    }

    @TargetApi(Build.VERSION_CODES.ECLAIR)
    private fun updateTastContacts(result: Result, conatcts: List<HashMap<String, Any>>, replace: Boolean) {
        println("updateContactsAsync call.method :: ${replace} ::");
        UpdateContactsTask(result, conatcts, replace).executeOnExecutor(executor, context);
    }

    override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
        //print(("onMethodCall call.method");
        //print("onMethodCall call.method ${call.method}");
        if (call.method == "getPlatformVersion") {
            result.success("Android ${android.os.Build.VERSION.RELEASE}")
        } else if (call.method == "getContacts") {
            //loadContacts();
            this.getContacts(result)
        } else if (call.method == "updateContacts") {
            val contact: List<HashMap<String, Any>>? = call.argument("contacts");// as List<HashMap<String, Any>>;
            val replaceOld: Boolean? = call.argument("replaceOld");//as Boolean;
            if (this.updateContacts(contact!!, replaceOld = replaceOld!!)) {
                result.success(null)
            } else {
                result.error(null, "Failed to update the contact, make sure it has a valid identifier", null)
            }
        } else if (call.method == "updateContactsAsync") {
            val contact: List<HashMap<String, Any>>? = call.argument("contacts");// as List<HashMap<String, Any>>;
            val replaceOld: Boolean? = call.argument("replaceOld");//as Boolean;
            this.updateTastContacts(result, contact!!, replaceOld!!)
            /*if (this.updateContacts(contact!!, replaceOld = replaceOld!!)) {
                result.success(null)
            } else {
                result.error(null, "Failed to update the contact, make sure it has a valid identifier", null)
            }*/
        } else if (call.method == "updateContact") {
            //println(("updateContact call.method");
            val contact: HashMap<String, Any>? = call.argument("contact");// as HashMap<String, Any>;
            val replaceOld: Boolean? = call.argument("replaceOld");// as Boolean;
            if (this.updateContact(contact!!, replaceOld = replaceOld!!)) {
                result.success(true)
            } else {
                result.error(null, "Failed to update the contact, make sure it has a valid identifier", null)
            }
        } else {
            result.notImplemented()
        }
    }

    private fun updateContact(contact: HashMap<String, Any>, replaceOld: Boolean = true): Boolean {
        //println(("updateContact here");

        //println((contact);
        //println((contact["contactId"]);
        val contentResolver: ContentResolver = context.getContentResolver()
        var contactsHelper: ContactsGetterBuilder = ContactsGetterBuilder(context)
        var datas: List<ContactData> = contactsHelper.allFields().buildList();

        // check contacts
        var data: ContactData? = datas.filter { e ->
            e.contactId == (contact["contactId"] as Int)
                    && e.lookupKey == (contact["lookupKey"] as String)
        }.firstOrNull();
        if (data != null) {

            //println(("Found :::: >");
            //println((data);
            //println((data.accountName);
            //println((data.compositeName);
            //println((data.isFavorite);
            //println((data.nickName);
            //println((data.nameData.firstName);
            //println((data.nameData.fullName);
            //println((data.nameData.middleName);
            //println((data.nameData.surname);
            //println((data.contactId);
            //println((data.lookupKey);

            //contact["contactId"]

            if (replaceOld) {

                data.phoneList.forEachIndexed { index, phoneNumber ->
                    updatePhone(data.contactId.toLong(), phoneNumber.mainData, (contact["phoneList"] as List<HashMap<String, Any>>)[index]["mainData"] as String);
                }
            } else {
                data.phoneList = data.phoneList.mapIndexed { index, phoneNumber ->
                    //println("data.phoneList.size ${data.phoneList.size}");
                    //println("data.phoneList.mapIndexed $index");
                    //println("contact[\"phoneList\"]  ${contact["phoneList"].toString()}");

                    phoneNumber.mainData = (contact["phoneList"] as List<HashMap<String, Any>>)[index]["mainData"] as String;
                    phoneNumber;
                }.toList();

                var contactsUpdate: ContactsUpdaterBuilder = ContactsUpdaterBuilder(context)

                val result = contactsUpdate.updateContact(data);

                //print(result);
            }

            /*(contact["phoneList"] as List<HashMap<String, Any>>).map { p ->
        val phone:PhoneNumber? =null;
        if (p["labelName"] != null)
            PhoneNumber((p["mainData"] as String), (p["labelName"] as String));
        else
            PhoneNumber(context, (p["mainData"] as String), data.contactId);

        if(phone!=null){
            phone!!.contactId = p["contactId"] as Int;
            phone.labelId = p["labelId"] as Int;
            phone.labelName = p["labelName"] as String;
        }
        phone;
    }.toList();*/


        } else {
            return false;
        }

        return true;
    }

    private fun updatePhone(contactId: Long, existingNumber: String, newNumber: String) {
        val contentValues = ContentValues()
        contentValues.put(ContactsContract.CommonDataKinds.Phone.NUMBER, newNumber)

        val where = ContactsContract.Data.CONTACT_ID + "=?" + " AND " + ContactsContract.Data.MIMETYPE + "=?" + " AND " + ContactsContract.CommonDataKinds.Phone.NUMBER + "=?"
        val whereArgs = arrayOf<String>((contactId).toString(), ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, existingNumber)

        context.contentResolver.update(ContactsContract.Data.CONTENT_URI, contentValues, where, whereArgs)
    }

    /**
     * | contactId , lookupKey , phoneList : List<WithLabel | structure>
     */
    private fun updateContacts(contacts: List<HashMap<String, Any>>, replaceOld: Boolean = true): Boolean {
        //print("updateContacts here");

        val datasNewContacts: MutableList<ContactData> = ArrayList<ContactData>();
        val contentResolver: ContentResolver = context.getContentResolver()
        var contactsHelper: ContactsGetterBuilder = ContactsGetterBuilder(context)
        var contactsUpdate: ContactsUpdaterBuilder = ContactsUpdaterBuilder(context)

        var datas: List<ContactData> = contactsHelper.allFields().buildList();

        // check contacts
        for (item: HashMap<String, Any> in contacts) {

            // check contacts
            var data: ContactData? = datas.filter { e ->
                e.contactId == (item["contactId"] as Int)
                        && e.lookupKey == (item["lookupKey"] as String)
            }.firstOrNull();
            //print(item);
            //print(data);

            if (data != null) {
                if (replaceOld) {

                    data.phoneList.forEachIndexed { index, phoneNumber ->
                        updatePhone(data.contactId.toLong(), phoneNumber.mainData, (item["phoneList"] as List<HashMap<String, Any>>)[index]["mainData"] as String);
                    }
                } else {
                    data.phoneList = data.phoneList.mapIndexed { index, phoneNumber ->
                        phoneNumber.mainData = (item["phoneList"] as List<HashMap<String, Any>>)[index]["mainData"] as String;
                        phoneNumber;
                    }.toList();

                    datasNewContacts.add(data);
                }
            }

        }

        if (!replaceOld && datasNewContacts.size > 0) {
            var contactsUpdate: ContactsUpdaterBuilder = ContactsUpdaterBuilder(context)

            val result = contactsUpdate.updateContactsList(datasNewContacts);

            //print(result);
        }

        return true;
    }

    /* private fun loadContacts() {
         disposable.dispose()
         disposable = contactsHelper.getAllContacts().subscribeOn(Schedulers.io())
                 .observeOn(AndroidSchedulers.mainThread())
                 .subscribe({
                     adapter.bindItem(it.values.toList())
                 }, { Log.e("ContactHelper", it.message, it) })
     }*/


    override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        channel.setMethodCallHandler(null)
    }

    override fun onDetachedFromActivity() {
        TODO("Not yet implemented")
    }

    override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
        TODO("Not yet implemented")
    }

    override fun onAttachedToActivity(binding: ActivityPluginBinding) {
        activity = binding.activity;
    }

    override fun onDetachedFromActivityForConfigChanges() {
        TODO("Not yet implemented")
    }

    @TargetApi(Build.VERSION_CODES.CUPCAKE)
    private class GetContactsTask(result: Result) : AsyncTask<Any?, Void?, MutableList<HashMap<String, Any>>?>() {
        private val getContactResult: Result

        override fun onPostExecute(result: MutableList<HashMap<String, Any>>?) {
            if (result == null) {
                getContactResult.notImplemented()
            } else {
                getContactResult.success(result)
            }
        }

        init {
            getContactResult = result
        }

        @TargetApi(Build.VERSION_CODES.ECLAIR)
        override fun doInBackground(vararg params: Any?): MutableList<HashMap<String, Any>>? {
            val contacts: MutableList<HashMap<String, Any>>?
            contacts = getContactsData(params.get(0) as Context);

            return contacts!!;
        }


        private fun getContactsData(context: Context): MutableList<HashMap<String, Any>> {
            val contentResolver: ContentResolver = context.getContentResolver()
            var contactsHelper: ContactsGetterBuilder = ContactsGetterBuilder(context)
            var datas: List<ContactData> = contactsHelper.allFields().buildList();
            var dataReturn: MutableList<HashMap<String, Any>> = ArrayList<HashMap<String, Any>>();


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                dataReturn = datas.stream().map { it ->

                    val contactMap = HashMap<String, Any>();
                    contactMap.put("contactId", it.contactId);
                    contactMap.put("lookupKey", it.lookupKey);
                    contactMap.put("note", it.note ?: "");
                    contactMap.put("nickName", it.nickName ?: "");
                    contactMap.put("sipAddress", it.sipAddress ?: "");
                    contactMap.put("compositeName", it.compositeName ?: "");
                    contactMap.put("accountName", it.accountName ?: "");
                    contactMap.put("accountType", it.accountType ?: "");
                    contactMap.put("isFavorite", it.isFavorite);

                    contactMap.put("lastModificationDate", it.lastModificationDate);
                    //contactMap.put("updatedPhotoUri", it.updatedPhotoUri);
                    contactMap.put("updatedBitmap", it.updatedBitmap);

                    val nameData = HashMap<String, Any>();
                    nameData.put("firstName", it.nameData.firstName);
                    nameData.put("fullName", it.nameData.fullName);
                    nameData.put("middleName", it.nameData.middleName);
                    nameData.put("namePrefix", it.nameData.namePrefix);
                    nameData.put("nameSuffix", it.nameData.nameSuffix);
                    nameData.put("phoneticFirst", it.nameData.phoneticFirst);
                    nameData.put("phoneticLast", it.nameData.phoneticLast);
                    nameData.put("phoneticMiddle", it.nameData.phoneticMiddle);
                    nameData.put("surname", it.nameData.surname);
                    contactMap.put("nameData", nameData);


                    val addressesList: List<HashMap<String, Any>> = it.addressesList.stream().map { t ->
                        val p = HashMap<String, Any>();
                        p.put("mainData", t.mainData);
                        p.put("contactId", t.contactId);
                        p.put("labelId", t.labelId);
                        p.put("labelName", t.labelName);
                        p;
                    }.collect(Collectors.toList());

                    contactMap.put("addressesList", addressesList);


                    contactMap.put("websitesList", it.websitesList);


                    val imAddressesList: List<HashMap<String, Any>> = it.imAddressesList.stream().map { t ->
                        val p = HashMap<String, Any>();
                        p.put("mainData", t.mainData);
                        p.put("contactId", t.contactId);
                        p.put("labelId", t.labelId);
                        p.put("labelName", t.labelName);
                        p;
                    }.collect(Collectors.toList());

                    contactMap.put("imAddressesList", imAddressesList);


                    val relationsList: List<HashMap<String, Any>> = it.relationsList.stream().map { t ->
                        val p = HashMap<String, Any>();
                        p.put("mainData", t.mainData);
                        p.put("contactId", t.contactId);
                        p.put("labelId", t.labelId);
                        p.put("labelName", t.labelName);
                        p;
                    }.collect(Collectors.toList());

                    contactMap.put("relationsList", relationsList);


                    val specialDatesList: List<HashMap<String, Any>> = it.specialDatesList.stream().map { t ->
                        val p = HashMap<String, Any>();
                        p.put("mainData", t.mainData);
                        p.put("contactId", t.contactId);
                        p.put("labelId", t.labelId);
                        p.put("labelName", t.labelName);
                        p;
                    }.collect(Collectors.toList());

                    contactMap.put("specialDatesList", specialDatesList);


                    val groupList: List<HashMap<String, Any>> = it.groupList.stream().map { t ->
                        val p = HashMap<String, Any>();
                        p.put("groupId", t.groupId);
                        p.put("groupTitle", t.groupTitle);
                        p;
                    }.collect(Collectors.toList());

                    contactMap.put("groupList", groupList);

                    val organization = HashMap<String, Any>();
                    organization.put("name", it.organization.name);
                    organization.put("title", it.organization.title);
                    contactMap.put("organization", organization);


                    val phones: List<HashMap<String, Any>> = it.phoneList.stream().map { phone ->
                        val p = HashMap<String, Any>();
                        ////print(phone.mainData);

                        p.put("mainData", phone.mainData);
                        p.put("contactId", phone.contactId);
                        p.put("labelId", phone.labelId);
                        p.put("labelName", phone.labelName);
                        p;
                    }.collect(Collectors.toList());


                    contactMap.put("phoneList", phones);

                    val emails: List<HashMap<String, Any>> = it.emailList.stream().map { e ->
                        val p = HashMap<String, Any>();
                        p.put("mainData", e.mainData);
                        p.put("contactId", e.contactId);
                        p.put("labelId", e.labelId);
                        p.put("labelName", e.labelName);
                        p.put("customLabelId", e.customLabelId);
                        p;
                    }.collect(Collectors.toList());
                    contactMap.put("emailList", emails);
                    contactMap;

                }.collect(Collectors.toList());

                // do fillter code here
            } else {
                //show some toast message or thing you need to do

                datas.forEach {
                    val contactMap = HashMap<String, Any>();
                    contactMap.put("contactId", it.contactId);
                    contactMap.put("lookupKey", it.lookupKey);
                    contactMap.put("note", it.note ?: "");
                    contactMap.put("nickName", it.nickName ?: "");
                    contactMap.put("sipAddress", it.sipAddress ?: "");
                    contactMap.put("compositeName", it.compositeName ?: "");
                    contactMap.put("accountName", it.accountName ?: "");
                    contactMap.put("accountType", it.accountType ?: "");
                    contactMap.put("isFavorite", it.isFavorite);

                    contactMap.put("lastModificationDate", it.lastModificationDate);
                    //contactMap.put("updatedPhotoUri", it.updatedPhotoUri);
                    contactMap.put("updatedBitmap", it.updatedBitmap);

                    val nameData = HashMap<String, Any>();
                    nameData.put("firstName", it.nameData.firstName);
                    nameData.put("fullName", it.nameData.fullName);
                    nameData.put("middleName", it.nameData.middleName);
                    nameData.put("namePrefix", it.nameData.namePrefix);
                    nameData.put("nameSuffix", it.nameData.nameSuffix);
                    nameData.put("phoneticFirst", it.nameData.phoneticFirst);
                    nameData.put("phoneticLast", it.nameData.phoneticLast);
                    nameData.put("phoneticMiddle", it.nameData.phoneticMiddle);
                    nameData.put("surname", it.nameData.surname);
                    contactMap.put("nameData", nameData);


                    val addressesList: MutableList<HashMap<String, Any>> = ArrayList();
                    it.addressesList.forEach {t->
                        val p = HashMap<String, Any>();
                        p.put("mainData", t.mainData);
                        p.put("contactId", t.contactId);
                        p.put("labelId", t.labelId);
                        p.put("labelName", t.labelName);
                        addressesList.add(p);
                    }
                    contactMap.put("addressesList", addressesList);


                    contactMap.put("websitesList", it.websitesList);


                    val imAddressesList: MutableList<HashMap<String, Any>> =  ArrayList();
                    it.imAddressesList.forEach {t->
                        val p = HashMap<String, Any>();
                        p.put("mainData", t.mainData);
                        p.put("contactId", t.contactId);
                        p.put("labelId", t.labelId);
                        p.put("labelName", t.labelName);
                        imAddressesList.add(p);
                    }

                    contactMap.put("imAddressesList", imAddressesList);


                    val relationsList: MutableList<HashMap<String, Any>> =ArrayList();
                    it.relationsList.forEach {t->
                        val p = HashMap<String, Any>();
                        p.put("mainData", t.mainData);
                        p.put("contactId", t.contactId);
                        p.put("labelId", t.labelId);
                        p.put("labelName", t.labelName);
                        relationsList.add(p);
                    };

                    contactMap.put("relationsList", relationsList);


                    val specialDatesList: MutableList<HashMap<String, Any>> =ArrayList();
                    it.specialDatesList.forEach {t->
                        val p = HashMap<String, Any>();
                        p.put("mainData", t.mainData);
                        p.put("contactId", t.contactId);
                        p.put("labelId", t.labelId);
                        p.put("labelName", t.labelName);
                        specialDatesList.add(p);
                    };

                    contactMap.put("specialDatesList", specialDatesList);


                    val groupList: MutableList<HashMap<String, Any>> =ArrayList();
                    it.groupList.forEach {t->
                        val p = HashMap<String, Any>();
                        p.put("groupId", t.groupId);
                        p.put("groupTitle", t.groupTitle);
                        groupList.add(p);
                    };

                    contactMap.put("groupList", groupList);

                    val organization = HashMap<String, Any>();
                    organization.put("name", it.organization.name);
                    organization.put("title", it.organization.title);
                    contactMap.put("organization", organization);


                    val phones: MutableList<HashMap<String, Any>> = ArrayList();
                    it.phoneList.forEach {t->
                        val p = HashMap<String, Any>();
                        ////print(phone.mainData);

                        p.put("mainData", t.mainData);
                        p.put("contactId", t.contactId);
                        p.put("labelId", t.labelId);
                        p.put("labelName", t.labelName);
                        phones.add(p);
                    };


                    contactMap.put("phoneList", phones);

                    val emails: MutableList<HashMap<String, Any>> = ArrayList();
                    it.emailList.forEach {e->
                        val p = HashMap<String, Any>();
                        p.put("mainData", e.mainData);
                        p.put("contactId", e.contactId);
                        p.put("labelId", e.labelId);
                        p.put("labelName", e.labelName);
                        p.put("customLabelId", e.customLabelId);
                        emails.add(p);
                    };
                    contactMap.put("emailList", emails);
                    dataReturn.add(contactMap);
                };
            }

            //println(("${dataReturn.size}");
            return dataReturn;

        }

    }


    @TargetApi(Build.VERSION_CODES.CUPCAKE)
    private class UpdateContactsTask(result: Result, cs: List<HashMap<String, Any>>, replace: Boolean = true) : AsyncTask<Any?, Void?, Boolean?>() {
        private val updateContactResult: Result
        private val contacts: List<HashMap<String, Any>>
        private val replaceOld: Boolean

        override fun onPostExecute(result: Boolean?) {
            if (result == null) {
                updateContactResult.notImplemented()
            } else {
                updateContactResult.success(result)
            }
        }

        init {
            contacts = cs

            updateContactResult = result

            replaceOld = replace
        }

        @TargetApi(Build.VERSION_CODES.ECLAIR)
        override fun doInBackground(vararg params: Any?): Boolean? {
            //print("updateContacts doInBackground");
            val res: Boolean = updateContactsData(params.get(0) as Context)
            return res;
        }


        private fun updateContactsData(context: Context): Boolean {


            val datasNewContacts: MutableList<ContactData> = ArrayList<ContactData>();
            val contentResolver: ContentResolver = context.getContentResolver()
            var contactsHelper: ContactsGetterBuilder = ContactsGetterBuilder(context)
            var contactsUpdate: ContactsUpdaterBuilder = ContactsUpdaterBuilder(context)

            var datas: List<ContactData> = contactsHelper.allFields().buildList();

            // check contacts
            for (item: HashMap<String, Any> in contacts) {

                // check contacts
                var data: ContactData? = datas.filter { e ->
                    e.contactId == (item["contactId"] as Int)
                            && e.lookupKey == (item["lookupKey"] as String)
                }.firstOrNull();
                //print(item);
                //print(data);

                if (data != null) {
                    if (replaceOld) {

                        data.phoneList.forEachIndexed { index, phoneNumber ->
                            updatePhone(data.contactId.toLong(), phoneNumber.mainData, (item["phoneList"] as List<HashMap<String, Any>>)[index]["mainData"] as String, context);
                        }
                    } else {
                        data.phoneList = data.phoneList.mapIndexed { index, phoneNumber ->
                            phoneNumber.mainData = (item["phoneList"] as List<HashMap<String, Any>>)[index]["mainData"] as String;
                            phoneNumber;
                        }.toList();

                        datasNewContacts.add(data);
                    }
                }

            }
            if (!replaceOld && datasNewContacts.size > 0) {
                var contactsUpdate: ContactsUpdaterBuilder = ContactsUpdaterBuilder(context)

                val result = contactsUpdate.updateContactsList(datasNewContacts);

                //print(result);
            }

            return true;
        }

        private fun updatePhone(contactId: Long, existingNumber: String, newNumber: String, context: Context) {
            val contentValues = ContentValues()
            contentValues.put(ContactsContract.CommonDataKinds.Phone.NUMBER, newNumber)

            val where = ContactsContract.Data.CONTACT_ID + "=?" + " AND " + ContactsContract.Data.MIMETYPE + "=?" + " AND " + ContactsContract.CommonDataKinds.Phone.NUMBER + "=?"
            val whereArgs = arrayOf<String>((contactId).toString(), ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, existingNumber)

            context.contentResolver.update(ContactsContract.Data.CONTENT_URI, contentValues, where, whereArgs)
        }

    }

}
