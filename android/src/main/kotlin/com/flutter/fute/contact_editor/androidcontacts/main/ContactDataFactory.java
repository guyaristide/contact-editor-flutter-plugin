package com.flutter.fute.contact_editor.androidcontacts.main;

import com.flutter.fute.contact_editor.androidcontacts.entity.ContactData;

public class ContactDataFactory {
    /**
     * @return Creates empty contact data object
     */
    public static ContactData createEmpty() {
        return new ContactData() {
        };
    }
}
