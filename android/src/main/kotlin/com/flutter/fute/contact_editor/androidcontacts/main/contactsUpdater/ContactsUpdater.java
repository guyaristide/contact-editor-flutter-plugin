package com.flutter.fute.contact_editor.androidcontacts.main.contactsUpdater;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import com.flutter.fute.contact_editor.androidcontacts.entity.ContactData;
import com.flutter.fute.contact_editor.androidcontacts.entity.PhoneNumber;
import com.flutter.fute.contact_editor.androidcontacts.interfaces.WithLabel;

import java.util.ArrayList;
import java.util.List;

class ContactsUpdater {
    private ContentResolver mResolver;

    public ContactsUpdater(ContentResolver resolver) {
        mResolver = resolver;
    }

    public int[] updateContacts(List<ContactData> contactDataList) {
        ArrayList<ContentValues> cvList = new ArrayList<>(100);

         int[] ids = new int[contactDataList.size()];
        for (int i = 0; i < contactDataList.size(); i++) {
            Uri uri =   Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, contactDataList.get(i).getLookupKey());
            //int id = Integer.parseInt(uri.getLastPathSegment());

            //Get contact raw ID
            int id = getRawContactId(contactDataList.get(i).getContactId()+"");

            //System.out.println("Contact ID "+contactDataList.get(i).getContactId());
            //System.out.println("Contact RAW ID "+id);

            ContactData contactData = contactDataList.get(i);
            for (PhoneNumber number : contactData.getPhoneList()) {
                cvList.add(getWithLabelCV(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, number, id));

                           }
            ids[i] = id;
        }
        mResolver.bulkInsert(ContactsContract.Data.CONTENT_URI, cvList.toArray(new ContentValues[cvList.size()]));
        return ids;
    }


    private ContentValues getWithLabelCV(String contentType, WithLabel withLabel, int id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ContactsContract.Data.RAW_CONTACT_ID, id);
        contentValues.put(ContactsContract.Data.MIMETYPE, contentType);
        contentValues.put(ContactsContract.Data.DATA1, withLabel.getMainData());
        contentValues.put(ContactsContract.Data.DATA2, withLabel.getLabelId());
        if (withLabel.getLabelId() == withLabel.getCustomLabelId())
            contentValues.put(ContactsContract.Data.DATA3, withLabel.getLabelName());
        return contentValues;
    }

    public int getRawContactId(String contactId)
    {
        String res = "";
        Uri uri = ContactsContract.RawContacts.CONTENT_URI;
        String[] projection = new String[]{ContactsContract.RawContacts._ID};
        String selection = ContactsContract.RawContacts.CONTACT_ID + " = ?";
        String[] selectionArgs = new String[]{ contactId };
        Cursor c = mResolver.query(uri, projection, selection, selectionArgs, null);

        if(c != null && c.moveToFirst())
        {
            res = c.getString(c.getColumnIndex(ContactsContract.RawContacts._ID));
            c.close();
        }

        return Integer.parseInt(res);
    }
}
