#import "ContactEditorPlugin.h"
#if __has_include(<contact_editor/contact_editor-Swift.h>)
#import <contact_editor/contact_editor-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "contact_editor-Swift.h"
#endif

@implementation ContactEditorPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftContactEditorPlugin registerWithRegistrar:registrar];
}
@end
