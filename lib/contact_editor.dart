import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';

class ContactEditor {
  static const MethodChannel _channel = const MethodChannel('contact_editor');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<bool> updateContact(Contact e, {bool replaceOld = true}) async {
    final bool result =
        await _channel.invokeMethod('updateContact', <String, dynamic>{
      "replaceOld": replaceOld,
      "contact": <String, dynamic>{
        "contactId": e.contactId,
        "lookupKey": e.lookupKey,
        "phoneList": e.phoneList.map((e) => e.toMap()).toList().cast<Map>()
      }
    });

    //print("result => $result");
    return result;
  }

  static Future<bool> updateContacts(List<Contact> contacts, {bool replaceOld = true}) async {
    List<Map> datas = contacts.map((e) {
      return {
        "contactId": e.contactId,
        "lookupKey": e.lookupKey,
        "phoneList": e.phoneList.map((e) => e.toMap()).toList()
      };
    }).toList();

    final bool result = await _channel.invokeMethod('updateContacts', <String, dynamic>{
      "replaceOld": replaceOld,
      "contacts": datas
    });

    return result;
  }

  static Future<List<Contact>> get getContacts async {
    // check permission
    Permission contactPermission = Permission.contacts;

    await contactPermission.request();

    if (await contactPermission.isDenied)
      throw "We don't have contact permission";
    final List<dynamic> contacts = await _channel.invokeMethod('getContacts');

    List<Contact> contactsRender = null;
    if (contacts != null)
      contactsRender = contacts.map((e) {
        return Contact.fromMap(e);
      }).toList();

    return contactsRender;
  }



  static Future<bool> updateContactsAsync(List<Contact> contacts, {bool replaceOld = true}) async {
    //print("----------${contacts.length}--------");
    List<Map> datas = contacts.map((e) {
      return {
        "contactId": e.contactId,
        "lookupKey": e.lookupKey,
        "phoneList": e.phoneList.map((e) => e.toMap()).toList()
      };
    }).toList();

    final bool result = await _channel.invokeMethod('updateContactsAsync', <String, dynamic>{
      "replaceOld": replaceOld,
      "contacts": datas
    });

    //print("result $result");
    return result;
  }

}

class Address {
  static final int TYPE_HOME = 1;
  static final int TYPE_WORK = 2;
  static final int TYPE_OTHER = 3;

  String mainData;
  int contactId;
  int labelId;
  String labelName;

  Address({this.mainData, this.contactId, this.labelId, this.labelName});

  Address.fromMap(Map m) {
    mainData = m["mainData"];
    contactId = m["contactId"];
    labelId = m["labelId"];
    labelName = m["labelName"];
  }

  static Map _toMap(Address withLabel) {
    return {
      "mainData": withLabel.mainData,
      "contactId": withLabel.contactId,
      "labelId": withLabel.labelId,
      "labelName": withLabel.labelName
    };
  }

  Map toMap() {
    return Address._toMap(this);
  }
}

class WithLabel {
  String mainData;
  int contactId;
  int labelId;
  String labelName;

  WithLabel({this.mainData, this.contactId, this.labelId, this.labelName});

  WithLabel.fromMap(Map m) {
    mainData = m["mainData"];
    contactId = m["contactId"];
    labelId = m["labelId"];
    labelName = m["labelName"];
  }

  static Map _toMap(WithLabel withLabel) {
    return {
      "mainData": withLabel.mainData,
      "contactId": withLabel.contactId,
      "labelId": withLabel.labelId,
      "labelName": withLabel.labelName
    };
  }

  Map toMap() {
    return WithLabel._toMap(this);
  }

  /// The [+] operator fills in this contact's empty fields with the fields from [other]
  operator +(WithLabel other) => WithLabel(
      mainData: this.mainData ?? other.mainData,
      contactId: this.contactId ?? other.contactId,
      labelId: this.labelId ?? other.labelId,
      labelName: this.labelName ?? other.labelName);

/*
  @override
  bool operator ==(WithLabel other) {
    return other is WithLabel &&
        this.mainData == other.mainData &&
        this.contactId == other.contactId &&
        this.labelId == other.labelId &&
        this.labelName == other.labelName;
  }*/
/*
  @override
  int get hashCode {
    return hashObjects([
      this.mainData,
      this.contactId,
      this.labelId,
      this.labelName,
    ].where((s) => s != null));
  }

   */
}

class SpecialDate {
  static final int TYPE_ANNIVERSARY = 1;
  static final int TYPE_OTHER = 2;
  static final int TYPE_BIRTHDAY = 3;

  String mainData;
  int contactId;
  int labelId;
  String labelName;

  SpecialDate({this.mainData, this.contactId, this.labelId, this.labelName});

  SpecialDate.fromMap(Map m) {
    mainData = m["mainData"];
    contactId = m["contactId"];
    labelId = m["labelId"];
    labelName = m["labelName"];
  }

  static Map _toMap(SpecialDate withLabel) {
    return {
      "mainData": withLabel.mainData,
      "contactId": withLabel.contactId,
      "labelId": withLabel.labelId,
      "labelName": withLabel.labelName
    };
  }

  Map toMap() {
    return SpecialDate._toMap(this);
  }
}

class Relation {
  static final int TYPE_ASSISTANT = 1;
  static final int TYPE_BROTHER = 2;
  static final int TYPE_CHILD = 3;
  static final int TYPE_DOMESTIC_PARTNER = 4;
  static final int TYPE_FATHER = 5;
  static final int TYPE_FRIEND = 6;
  static final int TYPE_MANAGER = 7;
  static final int TYPE_MOTHER = 8;
  static final int TYPE_PARENT = 9;
  static final int TYPE_PARTNER = 10;
  static final int TYPE_REFERRED_BY = 11;
  static final int TYPE_RELATIVE = 12;
  static final int TYPE_SISTER = 13;
  static final int TYPE_SPOUSE = 14;

  String mainData;
  int contactId;
  int labelId;
  String labelName;

  Relation({this.mainData, this.contactId, this.labelId, this.labelName});

  Relation.fromMap(Map m) {
    mainData = m["mainData"];
    contactId = m["contactId"];
    labelId = m["labelId"];
    labelName = m["labelName"];
  }

  static Map _toMap(Relation withLabel) {
    return {
      "mainData": withLabel.mainData,
      "contactId": withLabel.contactId,
      "labelId": withLabel.labelId,
      "labelName": withLabel.labelName
    };
  }

  Map toMap() {
    return Relation._toMap(this);
  }
}

class PhoneNumber {
  static final int TYPE_HOME = 1;
  static final int TYPE_MOBILE = 2;
  static final int TYPE_WORK = 3;
  static final int TYPE_FAX_WORK = 4;
  static final int TYPE_FAX_HOME = 5;
  static final int TYPE_PAGER = 6;
  static final int TYPE_OTHER = 7;
  static final int TYPE_CALLBACK = 8;
  static final int TYPE_CAR = 9;
  static final int TYPE_COMPANY_MAIN = 10;
  static final int TYPE_ISDN = 11;
  static final int TYPE_MAIN = 12;
  static final int TYPE_OTHER_FAX = 13;
  static final int TYPE_RADIO = 14;
  static final int TYPE_TELEX = 15;
  static final int TYPE_TTY_TDD = 16;
  static final int TYPE_WORK_MOBILE = 17;
  static final int TYPE_WORK_PAGER = 18;
  static final int TYPE_ASSISTANT = 19;
  static final int TYPE_MMS = 20;

  String mainData;
  int contactId;
  int labelId;
  String labelName;

  PhoneNumber({this.mainData, this.contactId, this.labelId, this.labelName});

  PhoneNumber.fromMap(Map m) {
    mainData = m["mainData"];
    contactId = m["contactId"];
    labelId = m["labelId"];
    labelName = m["labelName"];
  }

  static Map<String, dynamic> _toMap(PhoneNumber withLabel) {
    return <String, dynamic>{
      "mainData": withLabel.mainData,
      "contactId": withLabel.contactId,
      "labelId": withLabel.labelId,
      "labelName": withLabel.labelName
    };
  }

  Map toMap() {
    return PhoneNumber._toMap(this);
  }
}

class Organization {
  String name;
  String title;

  Organization({this.name, this.title});

  Organization.fromMap(Map m) {
    name = m["name"];
    title = m["title"];
  }

  static Map _toMap(Organization organization) {
    return {"name": organization.name, "title": organization.title};
  }

  Map toMap() {
    return Organization._toMap(this);
  }

  /// The [+] operator fills in this contact's empty fields with the fields from [other]
  operator +(Organization other) => Organization(
      name: this.name ?? other.name, title: this.title ?? other.title);
}

class NameData {
  String firstName;

  String surname;

  String namePrefix;

  String middleName;

  String nameSuffix;

  String phoneticFirst;

  String phoneticMiddle;

  String phoneticLast;

  NameData(
      {this.firstName = "",
      this.surname = "",
      this.namePrefix = "",
      this.middleName = "",
      this.nameSuffix = "",
      this.phoneticFirst = "",
      this.phoneticMiddle = "",
      this.phoneticLast = ""});

  NameData.fromMap(Map m) {
    firstName = m["firstName"];
    surname = m["surname"];
    namePrefix = m["namePrefix"];
    middleName = m["middleName"];
    nameSuffix = m["nameSuffix"];
    phoneticFirst = m["phoneticFirst"];
    phoneticMiddle = m["phoneticMiddle"];
    phoneticLast = m["phoneticLast"];
  }

  static Map _toMap(NameData nameData) {
    return {
      "firstName": nameData.firstName,
      "surname": nameData.surname,
      "namePrefix": nameData.namePrefix,
      "middleName": nameData.middleName,
      "nameSuffix": nameData.nameSuffix,
      "phoneticFirst": nameData.phoneticFirst,
      "phoneticMiddle": nameData.phoneticMiddle,
      "phoneticLast": nameData.phoneticLast
    };
  }

  Map toMap() {
    return NameData._toMap(this);
  }

  /// The [+] operator fills in this contact's empty fields with the fields from [other]
  operator +(NameData other) => NameData(
      firstName: this.firstName ?? other.firstName,
      surname: this.surname ?? other.surname,
      namePrefix: this.namePrefix ?? other.namePrefix,
      middleName: this.middleName ?? other.middleName,
      nameSuffix: this.nameSuffix ?? other.nameSuffix,
      phoneticFirst: this.phoneticFirst ?? other.phoneticFirst,
      phoneticMiddle: this.phoneticMiddle ?? other.phoneticMiddle,
      phoneticLast: this.phoneticLast ?? other.phoneticLast);

/*
  @override
  bool operator ==(NameData other) {
    return other is NameData &&
        this.mainData == other.mainData &&
        this.contactId == other.contactId &&
        this.labelId == other.labelId &&
        this.labelName == other.labelName;
  }*/
/*
  @override
  int get hashCode {
    return hashObjects([
      this.mainData,
      this.contactId,
      this.labelId,
      this.labelName,
    ].where((s) => s != null));
  }

   */
}

class IMAddress {
  String mainData;
  int contactId;
  int labelId;
  String labelName;

  IMAddress({this.mainData, this.contactId, this.labelId, this.labelName});

  IMAddress.fromMap(Map m) {
    mainData = m["mainData"];
    contactId = m["contactId"];
    labelId = m["labelId"];
    labelName = m["labelName"];
  }

  static Map _toMap(IMAddress withLabel) {
    return {
      "mainData": withLabel.mainData,
      "contactId": withLabel.contactId,
      "labelId": withLabel.labelId,
      "labelName": withLabel.labelName
    };
  }

  Map toMap() {
    return IMAddress._toMap(this);
  }
}

class Group {
  int groupId;

  String groupTitle;

  Group({this.groupId = -1, this.groupTitle = ""});

  Group.fromMap(Map m) {
    groupId = m["groupId"];
    groupTitle = m["groupTitle"];
  }

  static Map _toMap(Group group) {
    return {"groupId": group.groupId, "groupTitle": group.groupTitle};
  }

  Map toMap() {
    return Group._toMap(this);
  }

  /// The [+] operator fills in this contact's empty fields with the fields from [other]
  operator +(Group other) => Group(
      groupId: this.groupId ?? other.groupId,
      groupTitle: this.groupTitle ?? other.groupTitle);

/*
  @override
  bool operator ==(Group other) {
    return other is Group &&
        this.mainData == other.mainData &&
        this.contactId == other.contactId &&
        this.labelId == other.labelId &&
        this.labelName == other.labelName;
  }*/
/*
  @override
  int get hashCode {
    return hashObjects([
      this.mainData,
      this.contactId,
      this.labelId,
      this.labelName,
    ].where((s) => s != null));
  }

   */
}

class Email {
  static final int TYPE_HOME = 1;
  static final int TYPE_WORK = 2;
  static final int TYPE_OTHER = 3;
  static final int TYPE_MOBILE = 4;

  String mainData;
  int contactId;
  int labelId;
  String labelName;

  Email({this.mainData, this.contactId, this.labelId, this.labelName});

  Email.fromMap(Map m) {
    mainData = m["mainData"];
    contactId = m["contactId"];
    labelId = m["labelId"];
    labelName = m["labelName"];
  }

  static Map _toMap(Email withLabel) {
    return {
      "mainData": withLabel.mainData,
      "contactId": withLabel.contactId,
      "labelId": withLabel.labelId,
      "labelName": withLabel.labelName
    };
  }

  Map toMap() {
    return Email._toMap(this);
  }
}

class Contact {
  Contact(
      {this.contactId,
      this.lookupKey,
      this.note,
      this.nickName,
      this.sipAddress,
      this.photoUri,
      this.compositeName,
      this.accountName,
      this.accountType,
      this.updatedPhotoUri,
      this.nameData,
      this.organization,
      this.lastModificationDate,
      this.isFavorite,
      this.updatedBitmap,
      this.emailList,
      this.phoneList,
      this.addressesList,
      this.websitesList,
      this.imAddressesList,
      this.relationsList,
      this.specialDatesList,
      this.groupList});

  int contactId;
  String lookupKey,
      note,
      nickName,
      sipAddress,
      photoUri,
      compositeName,
      accountName,
      accountType,
      updatedPhotoUri;

  NameData nameData;
  Organization organization;
  int lastModificationDate;
  bool isFavorite;
  Uint8List updatedBitmap;
  List<Email> emailList;
  List<PhoneNumber> phoneList;
  List<Address> addressesList;
  List<String> websitesList;
  List<IMAddress> imAddressesList;
  List<Relation> relationsList;
  List<SpecialDate> specialDatesList;
  List<Group> groupList;

  Contact.fromMap(Map m) {
    contactId = m["contactId"];
    lookupKey = m["lookupKey"];
    note = m["note"];
    nickName = m["nickName"];
    sipAddress = m["sipAddress"];
    photoUri = m["photoUri"];
    compositeName = m["compositeName"];
    accountName = m["accountName"];
    accountType = m["accountType"];
    updatedPhotoUri = m["updatedPhotoUri"];
    nameData = m["nameData"] != null ? NameData.fromMap(m["nameData"]) : null;
    organization = m["organization"] != null
        ? Organization.fromMap(m["organization"])
        : null;
    lastModificationDate = m["lastModificationDate"];
    isFavorite = m["isFavorite"];
    updatedBitmap = m["updatedBitmap"];
    emailList = m["emailList"] == null
        ? null
        : (m["emailList"] as Iterable)
            ?.map((m) => WithLabel.fromMap(m))
            .toList()
            .cast<Email>();

    phoneList = m["phoneList"] == null
        ? null
        : (m["phoneList"] as Iterable)
            ?.map((m) => PhoneNumber.fromMap(m))
            .toList();
    addressesList = m["addressesList"] == null
        ? null
        : (m["addressesList"] as Iterable)
            ?.map((m) => WithLabel.fromMap(m))
            .toList()
            .cast<Address>();
    websitesList = m["websitesList"] != null
        ? (m["websitesList"] as Iterable)?.cast<String>()
        : null;
    imAddressesList = m["imAddressesList"] == null
        ? null
        : (m["imAddressesList"] as Iterable)
            ?.map((m) => WithLabel.fromMap(m))
            .toList()
            .cast<IMAddress>();
    relationsList = m["relationsList"] == null
        ? null
        : (m["relationsList"] as Iterable)
            ?.map((m) => WithLabel.fromMap(m))
            .toList()
            .cast<Relation>();
    specialDatesList = m["specialDatesList"] == null
        ? null
        : (m["specialDatesList"] as Iterable)
            ?.map((m) => WithLabel.fromMap(m))
            .toList()
            .cast<SpecialDate>();
    groupList = m["groupList"] == null
        ? null
        : (m["groupList"] as Iterable)?.map((m) => Group.fromMap(m)).toList();
  }

  static Map _toMap(Contact contact) {
    return {
      "contactId": contact.contactId,
      "lookupKey": contact.lookupKey,
      "note": contact.note,
      "nickName": contact.nickName,
      "sipAddress": contact.sipAddress,
      "photoUri": contact.photoUri,
      "compositeName": contact.compositeName,
      "accountName": contact.accountName,
      "accountType": contact.accountType,
      "updatedPhotoUri": contact.updatedPhotoUri,
      "nameData": contact.nameData,
      "organization": contact.organization,
      "lastModificationDate": contact.lastModificationDate,
      "isFavorite": contact.isFavorite,
      "updatedBitmap": contact.updatedBitmap,
      "emailList": contact.emailList,
      "phoneList": contact.phoneList,
      "addressesList": contact.addressesList,
      "websitesList": contact.websitesList,
      "imAddressesList": contact.imAddressesList,
      "relationsList": contact.relationsList,
      "specialDatesList": contact.specialDatesList,
      "groupList": contact.groupList
    };
  }

  Map toMap() {
    return Contact._toMap(this);
  }

  /// The [+] operator fills in this contact's empty fields with the fields from [other]
  operator +(Contact other) => Contact(
      contactId: this.contactId ?? other.contactId,
      lookupKey: this.lookupKey ?? other.lookupKey,
      note: this.note ?? other.note,
      nickName: this.nickName ?? other.nickName,
      sipAddress: this.sipAddress ?? other.sipAddress,
      photoUri: this.photoUri ?? other.photoUri,
      compositeName: this.compositeName ?? other.compositeName,
      accountName: this.accountName ?? other.accountName,
      accountType: this.accountType ?? other.accountType,
      updatedPhotoUri: this.updatedPhotoUri ?? other.updatedPhotoUri,
      nameData: this.nameData ?? other.nameData,
      organization: this.organization ?? other.organization,
      lastModificationDate:
          this.lastModificationDate ?? other.lastModificationDate,
      isFavorite: this.isFavorite ?? other.isFavorite,
      updatedBitmap: this.updatedBitmap ?? other.updatedBitmap,
      emailList: this.emailList ?? other.emailList,
      phoneList: this.phoneList ?? other.phoneList,
      addressesList: this.addressesList ?? other.addressesList,
      websitesList: this.websitesList ?? other.websitesList,
      imAddressesList: this.imAddressesList ?? other.imAddressesList,
      relationsList: this.relationsList ?? other.relationsList,
      specialDatesList: this.specialDatesList ?? other.specialDatesList,
      groupList: this.groupList ?? other.groupList);
}
