import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:contact_editor/contact_editor.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';
  List<Contact> _contacts;
  bool loading = false;

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    List<Contact> contacts;
    setState(() {
      loading = true;
    });
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await ContactEditor.platformVersion;
      contacts = await ContactEditor.getContacts;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
      _contacts = contacts;
      loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    //Size size = MediaQuery.of(context).size;
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: _contacts == null && loading
            ? Column(
                children: [
                  Center(
                    child: CircularProgressIndicator(),
                  )
                ],
              )
            : ListView.builder(
                itemBuilder: (context, index) {
                  Contact c = _contacts[index];
                  return ListTile(
                    title: Text(
                        "${c.nameData.firstName ?? c.nameData.surname ?? c.compositeName ?? c.nickName ?? c.accountName ?? ""}"),
                    subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        //Text("${c.lookupKey} | ${c.contactId}"),
                        //Text("${c.accountType} | ${c.accountName} "),
                        Text(
                            "${c.phoneList != null ? c.phoneList.map((e) => e.mainData).toList().join(";") : ""}"),
                      ],
                    ),
                    trailing: IconButton(
                      icon: Icon(Icons.system_update),
                      onPressed: () {
                        c.phoneList = c.phoneList.map((e) {
                          e.mainData = "${e.mainData}0099";
                          return e;
                        }).toList();
                        List<Contact>cs = List();
                        cs.add(c);
                        ContactEditor.updateContactsAsync(cs, replaceOld: false);
                      },
                    ),
                  );
                },
                itemCount: _contacts != null ? _contacts.length : 0,
              ),
      ),
    );
  }
}
